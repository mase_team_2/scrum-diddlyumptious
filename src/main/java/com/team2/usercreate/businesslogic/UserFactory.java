package com.team2.usercreate.businesslogic;

import com.team2.usercreate.entities.Administrator;
import com.team2.usercreate.entities.CustomerServiceRep;
import com.team2.usercreate.entities.NetworkManagementEngineer;
import com.team2.usercreate.entities.SupportEngineer;
import com.team2.usercreate.entities.User;

public class UserFactory {

	public User createUser(String userType) {
		if (userType.equals("Administrator")) {
			return new Administrator();
		} else if (userType.equals("Customer Service Rep")) {
			return new CustomerServiceRep();
		} else if (userType.equals("Network Management Engineer")) {
			return new NetworkManagementEngineer();
		} else if (userType.equals("Support Engineer")) {
			return new SupportEngineer();
		} else {
			return null;
		}
	}
}
