package com.team2.dataset.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_equipment")
public class UserEquipment implements EntityObject {

	@Id
	private int typeAllocationCode; // primary key

	private String marketingName;
	private String manufacturer;
	private String accessCapability;
	private String model;
	private String vendorName;
	private String ueType;
	private String os;
	private String inputMode;

	public int getTypeAllocationCode() {
		return typeAllocationCode;
	}

	public void setTypeAllocationCode(int typeAllocationCode) {
		this.typeAllocationCode = typeAllocationCode;
	}

	public String getMarketingName() {
		return marketingName;
	}

	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getAccessCapability() {
		return accessCapability;
	}

	public void setAccessCapability(String accessCapability) {
		this.accessCapability = accessCapability;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getUeType() {
		return ueType;
	}

	public void setUeType(String ueType) {
		this.ueType = ueType;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getInputMode() {
		return inputMode;
	}

	public void setInputMode(String inputMode) {
		this.inputMode = inputMode;
	}

	@Override
	public String toString() {
		return "UserEquipment [typeAllocationCode=" + typeAllocationCode
				+ ", marketingName=" + marketingName + ", manufacturer="
				+ manufacturer + ", accessCapability=" + accessCapability
				+ ", model=" + model + ", vendorName=" + vendorName
				+ ", ueType=" + ueType + ", os=" + os + ", inputMode="
				+ inputMode + "]";
	}

}
