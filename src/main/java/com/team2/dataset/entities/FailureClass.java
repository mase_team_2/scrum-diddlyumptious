package com.team2.dataset.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "failure_class")
public class FailureClass implements EntityObject {

	@Id
	private int failureClassID; // primary key
	
	private String description;

	public int getFailureClassID() {
		return failureClassID;
	}

	public void setFailureClassID(int failureClassID) {
		this.failureClassID = failureClassID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "FailureClass [FailureClassID=" + failureClassID
				+ ", description=" + description + "]";
	}

}
