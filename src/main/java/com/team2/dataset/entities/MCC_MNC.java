package com.team2.dataset.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(MCC_MNCKey.class)
@Table(name = "mcc_mnc")
public class MCC_MNC implements EntityObject, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -550527288243006101L;
	@Id
	private int mobileCountryCode; // primary key
	@Id
	private int mobileNetworkCode; // primary key

	private String country;
	private String operator;

	public int getMobileCountryCode() {
		return mobileCountryCode;
	}

	public void setMobileCountryCode(int mobileCountryCode) {
		this.mobileCountryCode = mobileCountryCode;
	}

	public int getMobileNetworkCode() {
		return mobileNetworkCode;
	}

	public void setMobileNetworkCode(int mobileNetworkCode) {
		this.mobileNetworkCode = mobileNetworkCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	@Override
	public String toString() {
		return "MCC_MNC [mobileCountryCode=" + mobileCountryCode
				+ ", mobileNetworkCode=" + mobileNetworkCode + ", country="
				+ country + ", operator=" + operator + "]";
	}

}
