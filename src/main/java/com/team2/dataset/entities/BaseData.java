package com.team2.dataset.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "base_data")
public class BaseData implements EntityObject, Serializable {

	private static final long serialVersionUID = 6926293664527712090L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private Date date;
	private int eventId;

	private int failureClass;
	private int userEquipment;
	private int market;
	private int operator;
	private int cellId;
	private int duration;
	private int causeCode;
	private String neVersion;
	private BigInteger imsi;
	private BigInteger hier3_ID;
	private BigInteger hier32_ID;
	private BigInteger hier321_ID;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getFailureClass() {
		return failureClass;
	}

	public void setFailureClass(int failureClass) {
		this.failureClass = failureClass;
	}

	public int getUserEquipment() {
		return userEquipment;
	}

	public void setUserEquipment(int userEquipment) {
		this.userEquipment = userEquipment;
	}

	public int getMarket() {
		return market;
	}

	public void setMarket(int market) {
		this.market = market;
	}

	public int getOperator() {
		return operator;
	}

	public void setOperator(int operator) {
		this.operator = operator;
	}

	public int getCellId() {
		return cellId;
	}

	public void setCellId(int cellId) {
		this.cellId = cellId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getCauseCode() {
		return causeCode;
	}

	public void setCauseCode(int causeCode) {
		this.causeCode = causeCode;
	}

	public String getNeVersion() {
		return neVersion;
	}

	public void setNeVersion(String neVersion) {
		this.neVersion = neVersion;
	}

	public BigInteger getImsi() {
		return imsi;
	}

	public void setImsi(BigInteger imsi) {
		this.imsi = imsi;
	}

	public BigInteger getHier3_ID() {
		return hier3_ID;
	}

	public void setHier3_ID(BigInteger hier3_ID) {
		this.hier3_ID = hier3_ID;
	}

	public BigInteger getHier32_ID() {
		return hier32_ID;
	}

	public void setHier32_ID(BigInteger hier32_ID) {
		this.hier32_ID = hier32_ID;
	}

	public BigInteger getHier321_ID() {
		return hier321_ID;
	}

	public void setHier321_ID(BigInteger hier321_ID) {
		this.hier321_ID = hier321_ID;
	}

	@Override
	public String toString() {
		return "BaseData [id=" + id + ", date=" + date + ", eventId=" + eventId
				+ ", failureClass=" + failureClass + ", userEquipment="
				+ userEquipment + ", market=" + market + ", operator="
				+ operator + ", cellId=" + cellId + ", duration=" + duration
				+ ", causeCode=" + causeCode + ", neVersion=" + neVersion
				+ ", imsi=" + imsi + ", hier3_ID=" + hier3_ID + ", hier32_ID="
				+ hier32_ID + ", hier321_ID=" + hier321_ID + "]";
	}
}
