package com.team2.dataset.businesslogic;

import java.math.BigInteger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;

import com.team2.dataset.entities.BaseData;
import com.team2.dataset.entities.EntityObject;
import com.team2.dataset.entities.EventCause;
import com.team2.dataset.entities.FailureClass;
import com.team2.dataset.entities.MCC_MNC;
import com.team2.dataset.entities.UserEquipment;

public class EntityObjectFactory {
	private static final String BASE_DATA = "Base Data";
	private static final String EVENT_CAUSE = "Event-Cause Table";
	private static final String FAILURE_CLASS_TABLE = "Failure Class Table";
	private static final String MCC_MNC = "MCC - MNC Table";
	private static final String UE_TABLE = "UE Table";

	public EntityObject createEntityObject(HSSFCell[] entityParameters,
			String dataTable) {

		// creates Base Data table
		if (dataTable.equals(BASE_DATA)) {
			return createBaseData(entityParameters);
		}

		// creates Event Cause table
		else if (dataTable.equals(EVENT_CAUSE)) {
			return createEventCause(entityParameters);
		}

		// creates Failure Class table
		else if (dataTable.equals(FAILURE_CLASS_TABLE)) {
			return createFailureClassTable(entityParameters);
		}

		// creates MCC MNC table
		else if (dataTable.equals(MCC_MNC)) {
			return createMCCMNCTable(entityParameters);

		} // creates MCC MNC table
		else if (dataTable.equals(UE_TABLE)) {
			return createUserEquipment(entityParameters);

		} else {
			return null;
		}
	}

	private BaseData createBaseData(HSSFCell[] entityParameters) {
		BaseData baseData = new BaseData();
		baseData.setDate(entityParameters[0].getDateCellValue());
		baseData.setEventId((int) entityParameters[1].getNumericCellValue());
		baseData.setFailureClass((int) entityParameters[2]
				.getNumericCellValue());
		baseData.setUserEquipment((int) entityParameters[3]
				.getNumericCellValue());
		baseData.setMarket((int) entityParameters[4].getNumericCellValue());
		baseData.setOperator((int) entityParameters[5].getNumericCellValue());
		baseData.setCellId((int) entityParameters[6].getNumericCellValue());
		baseData.setDuration((int) entityParameters[7].getNumericCellValue());
		baseData.setCauseCode((int) entityParameters[8].getNumericCellValue());
		baseData.setNeVersion(entityParameters[9].toString());
		// baseData.setImsi(new BigInteger(entityParameters[10].toString()));
		HSSFCell imsiCell = entityParameters[10];
		imsiCell.setCellType(Cell.CELL_TYPE_STRING);
		baseData.setImsi(new BigInteger(imsiCell.getStringCellValue()));

		// baseData.setHier3_ID(new
		// BigInteger(entityParameters[11].toString()));
		HSSFCell hier3_IDCell = entityParameters[11];
		hier3_IDCell.setCellType(Cell.CELL_TYPE_STRING);
		baseData.setHier3_ID(new BigInteger(hier3_IDCell.getStringCellValue()));

		// baseData.setHier32_ID(new
		// BigInteger(entityParameters[12].toString()));
		HSSFCell hier32_IDCell = entityParameters[12];
		hier32_IDCell.setCellType(Cell.CELL_TYPE_STRING);
		baseData.setHier32_ID(new BigInteger(hier32_IDCell.getStringCellValue()));

		// baseData.setHier321_ID(new
		// BigInteger(entityParameters[13].toString()));
		HSSFCell hier321_IDCell = entityParameters[13];
		hier321_IDCell.setCellType(Cell.CELL_TYPE_STRING);
		baseData.setHier321_ID(new BigInteger(hier321_IDCell
				.getStringCellValue()));

		return baseData;
	}

	private EventCause createEventCause(HSSFCell[] entityParameters) {
		EventCause eventCause = new EventCause();
		eventCause
				.setCauseCode((int) entityParameters[0].getNumericCellValue());
		eventCause.setEventId((int) entityParameters[1].getNumericCellValue());
		eventCause.setDescription(entityParameters[2].toString());
		System.out.println(eventCause);
		return eventCause;
	}

	private FailureClass createFailureClassTable(HSSFCell[] entityParameters) {
		FailureClass failureClass = new FailureClass();
		failureClass.setFailureClassID((int) entityParameters[0]
				.getNumericCellValue());
		failureClass.setDescription(entityParameters[1].toString());
		System.out.println(failureClass);
		return failureClass;
	}

	private MCC_MNC createMCCMNCTable(HSSFCell[] entityParameters) {
		MCC_MNC mcc_mnc = new MCC_MNC();
		mcc_mnc.setMobileCountryCode((int) entityParameters[0]
				.getNumericCellValue());
		mcc_mnc.setMobileNetworkCode((int) entityParameters[1]
				.getNumericCellValue());
		mcc_mnc.setCountry(entityParameters[2].toString());
		mcc_mnc.setOperator(entityParameters[3].toString());
		System.out.println(mcc_mnc);
		return mcc_mnc;
	}

	private UserEquipment createUserEquipment(HSSFCell[] entityParameters) {
		UserEquipment userEquipment = new UserEquipment();
		userEquipment.setTypeAllocationCode((int) entityParameters[0]
				.getNumericCellValue());
		userEquipment.setMarketingName(entityParameters[1].toString());
		userEquipment.setManufacturer(entityParameters[2].toString());
		userEquipment.setAccessCapability(entityParameters[3].toString());
		userEquipment.setModel(entityParameters[4].toString());
		userEquipment.setVendorName(entityParameters[5].toString());
		userEquipment.setUeType(entityParameters[6].toString());
		userEquipment.setOs(entityParameters[7].toString());
		userEquipment.setInputMode(entityParameters[8].toString());
		System.out.println(userEquipment);
		return userEquipment;
	}
}
