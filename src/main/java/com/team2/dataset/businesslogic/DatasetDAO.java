package com.team2.dataset.businesslogic;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.team2.dataset.entities.BaseData;
import com.team2.dataset.entities.EntityObject;
import com.team2.dataset.entities.EventCause;
import com.team2.dataset.entities.FailureClass;
import com.team2.dataset.entities.MCC_MNC;
import com.team2.dataset.entities.UserEquipment;

@Stateless
@LocalBean
public class DatasetDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public List<BaseData> getAllBaseData() {
		String sql = "select * from base_data;";
		List<BaseData> baseDataList = entityManager.createNativeQuery(sql,
				BaseData.class).getResultList();
		System.out.println(baseDataList.size());
		return baseDataList;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addData(EntityObject entityObject) {
		if (entityObject instanceof BaseData) {
			BaseData baseData = (BaseData) entityObject;
			entityManager.persist(baseData);
		} else if (entityObject instanceof MCC_MNC) {
			MCC_MNC mcc_mnc = (MCC_MNC) entityObject;
			entityManager.persist(mcc_mnc);
		} else if (entityObject instanceof FailureClass) {
			FailureClass failureClass = (FailureClass) entityObject;
			entityManager.persist(failureClass);
		} else if (entityObject instanceof UserEquipment) {
			UserEquipment userEquipment = (UserEquipment) entityObject;
			entityManager.persist(userEquipment);
		} else if (entityObject instanceof EventCause) {
			EventCause eventCause = (EventCause) entityObject;
			entityManager.persist(eventCause);
		}
	}
}
