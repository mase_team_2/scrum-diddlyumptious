// The root URL for the Restful services
var rootURL = "HTTP://localhost:8080/Team2Project/rest/dataset";

var findAll = function() {
	console.log("findAll");
	$.ajax({
		type : 'GET',
		url : rootURL,
		dataType : "json",// data type of response
		success : renderList
	});
};

var renderList = function(data) {
	// JAX-RS serializes an empty list as null and a 'collection of one' as an
	// object
	// not an array of one
	var list = data == null ? [] : (data instanceof Array ? data : [ data ]);

	$.each(list, function(index, baseData) {
		var date = new Date(baseData.date);

		var dateString = date.getUTCDay() + "/" + date.getUTCMonth() + "/"
				+ date.getUTCFullYear() + " " + date.getUTCHours() + ":"
				+ date.getUTCMinutes();

		$('#table_body').append(
				'<tr>' + '<td>' + baseData.id + '</td>' + '<td>' + dateString
						+ '</td>' + '<td>' + baseData.eventId + '</td>'
						+ '<td>' + baseData.failureClass + '</td>' + '<td>'
						+ baseData.userEquipment + '</td>' + '<td>'
						+ baseData.market + '</td>' + '<td>'
						+ baseData.operator + '</td>' + '<td>'
						+ baseData.cellId + '</td>' + '<td>'
						+ baseData.duration + '</td>' + '<td>'
						+ baseData.causeCode + '</td>' + '<td>'
						+ baseData.neVersion + '</td>' + '<td>' + baseData.imsi
						+ '</td>' + '<td>' + baseData.hier3_ID + '</td>'
						+ '<td>' + baseData.hier32_ID + '</td>' + '<td>'
						+ baseData.hier321_ID + '</td>' + '</tr>');
	});
};

// Retreive the base data list when the DOM is ready
$(document).ready(function() {
	findAll();
});