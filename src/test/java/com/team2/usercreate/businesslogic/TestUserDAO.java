package com.team2.usercreate.businesslogic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import com.team2.usercreate.businesslogic.UserDAO;
import com.team2.usercreate.entities.User;

public class TestUserDAO {

	//private static final String USER_ID = "WH01A";

	private UserDAO userDAO = new UserDAO();

	@Before
	public void injectMockEntityManager() throws Exception {
		EntityManager entityManager = mock(EntityManager.class);
		userDAO.setEntityManager(entityManager);
	}

	@Test
	public void testAddUser() {
		User user = new User();
		user.setId("JB01N");
		userDAO.addUser(user);

		ArgumentCaptor<User> addedUser = ArgumentCaptor.forClass(User.class);
		verify(userDAO.getEntityManager(), atLeastOnce()).persist(
				addedUser.capture());

		String id = addedUser.getValue().getId();
		assertEquals("JB01N", id);
	}

	@Test
	public void testGetUser() {
		User user = new User();
		user.setId("JB01N");

		when(userDAO.getEntityManager().find(User.class, user.getId()))
				.thenReturn(user);
		assertEquals(userDAO.getUser("JB01N"), user);
	}
	
	@Test
	public void testGetUserNotFound() {
		User user = new User();
		user.setId("AA01N");

		when(userDAO.getEntityManager().find(User.class, user.getId()))
				.thenReturn(null);
		assertNull(userDAO.getUser("AA01N"));
	}

}
