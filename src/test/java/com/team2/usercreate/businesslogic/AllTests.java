package com.team2.usercreate.businesslogic;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.team2.usercreate.entities.TestUser;

@RunWith(Suite.class)
@SuiteClasses({ TestUser.class, TestUserDAO.class, TestUserWS.class, TestUserFactory.class })
public class AllTests {

}
