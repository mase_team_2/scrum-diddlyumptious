package com.team2.usercreate.businesslogic;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.team2.usercreate.businesslogic.UserFactory;
import com.team2.usercreate.entities.Administrator;
import com.team2.usercreate.entities.CustomerServiceRep;
import com.team2.usercreate.entities.NetworkManagementEngineer;
import com.team2.usercreate.entities.SupportEngineer;
import com.team2.usercreate.entities.User;

public class TestUserFactory {
	private UserFactory userFactory;

	@Before
	public void setUp() throws Exception {
		userFactory = new UserFactory();
	}

	@Test
	public void testCreateAdministrator() {
		User admin = userFactory.createUser("Administrator");
		assertTrue(admin instanceof Administrator);
	}

	@Test
	public void testCreateCustomerServiceRep() {
		User csr = userFactory.createUser("Customer Service Rep");
		assertTrue(csr instanceof CustomerServiceRep);
	}

	@Test
	public void testCreateNetworkManagementEngineer() {
		User nme = userFactory.createUser("Network Management Engineer");
		assertTrue(nme instanceof NetworkManagementEngineer);
	}

	@Test
	public void testSupportEngineer() {
		User se = userFactory.createUser("Support Engineer");
		assertTrue(se instanceof SupportEngineer);
	}

	@Test
	public void testUnknownUserType() {
		User unknownUser = userFactory.createUser("Canteen Lady");
		assertNull(unknownUser);
	}

}
