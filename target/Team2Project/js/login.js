

var userLogin = function() {
	var userId = $("userId").val();
	var password = $("password").val();

	//user authentication 
	var responseMessage = authenticate(userId, password);
	
	//handle response  (may be handled on server side?)
	if (responseMessage == 'OKAY') { 
		alert("User login OK");
		window.location.replace("menu.html"); 
	} 
	else if (responseMessage == 'FAIL') {
		alert("No login");
		// append error message to login page
	} else { // may be redundent
		alert("Error " + responseMessage);
		// append error message to login page
	} 
};
 
var authenticate = function(userId, password) { 
	var responseText = '';

	$.ajax({ 
		type: 'GET', 
		url: "/Team2Project/rest/users/login/" + userId + "&" + password, 
		dataType: 'text', 
		contentType: 'text/plain', 
		async: false, 
		success: function(text) { 
			responseText = text; 
		}, 
		error: function(text) { 
			responseText = text; 
		} 
	}); 
	return responseText; 
};